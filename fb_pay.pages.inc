<?php

function fb_pay_admin_settings(){
	$form = array();
  
  $options = array(0 => t('<none>')) +
    fb_admin_get_app_options(FALSE);
  if (count($options) == 1) {
    $message = t('You must create an app first!');
    drupal_set_message($message, 'error');
    return array('help' => array('#value' => $message));
  }

  $form['facebook_pay_app'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Pay Application'),
    '#description' => t('Need one app for pay.'),
    '#default_value' => variable_get('facebook_pay_app', NULL),
  );
	
	unset($options);
	
	$img_path = drupal_get_path('module', 'fb_pay') . '/images';
  $results = file_scan_directory($img_path, '.png');
  
  $options = array();
  foreach ($results as $image) {
    $options[$image->basename] = theme('image', $image->filename);
  }

  $form['facebook_pay_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Select pay button'),
    '#options' => $options,
    '#default_value' => variable_get('facebook_pay_button', 'Pay-with-Facebook-lighter.png'),
  );

  $form['facebook_page_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Authorization page open in'),
    '#options' => array('same page', 'popup'),
    '#default_value' => variable_get('facebook_page_button', '1'),
  );

  return system_settings_form($form);
}

function fb_pay_generate_key(){
	//print referer_uri();
	if (referer_uri()){
			$uri = explode("http://".$_SERVER['HTTP_HOST'],referer_uri());
			$node_url = explode("/",drupal_lookup_path('source', substr($uri[1], 1)));
	}else{
		$node_url = arg();
	}

 	$node = node_load($node_url[1]);
 	$title = "fb_" . truncate_utf8(strtolower($node->title), 5,TRUE);
	$_SESSION['n'] = $node_url[1];

	return $title;
}